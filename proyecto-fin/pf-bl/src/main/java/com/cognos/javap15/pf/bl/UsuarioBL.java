/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.pf.bl;

import com.cognos.javap15.pf.model.Medico;
import com.cognos.javap15.pf.model.Paciente;
import com.cognos.javap15.pf.model.Usuario;
import java.util.List;

/**
 *
 * @author JAVA
 */
public interface UsuarioBL {
    
    List<Usuario> listarUsuarios();
    
    Usuario registrarUsuario(Usuario usuario);
    
    Usuario modificarUsuario(Usuario usuario);
    
    Medico registrarMedico(Medico medico);
    
    Paciente registrarPaciente(Paciente paciente,Medico medico);
    
    Paciente registrarPaciente(Paciente paciente);
    
    Paciente modificarPaciente(Paciente paciente);
}
