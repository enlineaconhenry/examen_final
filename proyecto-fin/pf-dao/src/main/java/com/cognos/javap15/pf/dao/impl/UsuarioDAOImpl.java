/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.pf.dao.impl;

import com.cognos.javap15.pf.dao.UsuarioDAO;
import com.cognos.javap15.pf.model.Medico;
import com.cognos.javap15.pf.model.Paciente;
import com.cognos.javap15.pf.model.Usuario;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 *
 * @author JAVA
 */
@Stateless
public class UsuarioDAOImpl implements UsuarioDAO{

    @PersistenceContext(unitName = "proy-fin")
    EntityManager em;
    
    @Override
    public List<Usuario> listarUsuarios() {
        return em.createNamedQuery(Usuario.LISTAR_USUARIOS).getResultList();
    }

    @Override
    public Usuario registrarUsuario(Usuario usuario) {
        em.persist(usuario);
        return usuario;
    }

    @Override
    @Transactional(rollbackOn = {Exception.class})
    public synchronized Usuario modificarUsuario(Usuario usuario) {
        try {
            System.out.println("Obtener usuario " + usuario.getId());
            Usuario usuarioDB = em.find(Usuario.class, usuario.getId());
            usuarioDB.setNombre(usuario.getNombre());
            usuarioDB.setApellido(usuario.getApellido());
            usuarioDB.setLogin(usuario.getLogin());
            usuarioDB.setEmail(usuario.getEmail());
            em.flush();
            throw new Exception("Error provocado");
            // usuario = em.merge(usuarioDB);
            //return usuario;        
        } catch (Exception ex) {
            Logger.getLogger(UsuarioDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return usuario;
    }

    @Override
    public Medico registrarMedico(Medico medico) {
           Medico medico1 = (Medico) em.find(Medico.class, medico.getIdMedicoPk());
            em.flush();
        return medico1;
    }

    @Override
    public Paciente registrarPaciente(Paciente paciente, Medico medico) {
        return null;
    }

    @Override
    public Paciente registrarPaciente(Paciente paciente) {
         em.persist(paciente);
         return paciente;
    }

    @Override
    public Paciente modificarPaciente(Paciente paciente) {
        em.merge(paciente);
        return paciente;
    }

    @Override
    public Medico modificarMedico(Medico medico) {
        em.merge(medico);
        return medico;
    }
 
}
