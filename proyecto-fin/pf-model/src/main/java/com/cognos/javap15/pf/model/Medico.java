/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.pf.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author Henry-PC
 */

@Entity
@Table(name = "MEDICO")
public class Medico implements Serializable{
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)   
    @Column(name = "ID_MEDICO_PK")
    private String idMedicoPk;
    
    @Column(name = "NOMBRE")
    private String nombre;
    
    @Column(name = "APELLIDOS")
    private String apellidos;
    
    @Column(name = "TELEFONO")
    private String telefono;
    
    @Column(name = "ESPECIALIDAD")
    private String especialidad;
    
    @Column(name = "CODIGO")
    private String codigo;
    
    
    @JoinTable(
        name = "rel_med_pac",
        joinColumns = @JoinColumn(name = "ID_MEDICO_PK", nullable = true),
        inverseJoinColumns = @JoinColumn(name="ID_PACIENTE_PK", nullable = true)
    )
    @ManyToMany(cascade = CascadeType.ALL)
    private List<Paciente> pacientes;
    public Medico() {
    }

    public List<Paciente> getPacientes() {
        return pacientes;
    }

    public void setPacientes(List<Paciente> pacientes) {
        this.pacientes = pacientes;
    }

    public Medico(String idMedicoPk, String nombre) {
        this.idMedicoPk = idMedicoPk;
        this.nombre = nombre;
    }
    
    

    public String getIdMedicoPk() {
        return idMedicoPk;
    }

    public void setIdMedicoPk(String idMedicoPk) {
        this.idMedicoPk = idMedicoPk;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }   
}
