/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.pf.model;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author Henry-PC
 */
@Entity
@Table(name = "ATIENDE")
public class Atiende implements Serializable {
    @Id
    @Column(name = "ID_ATIENDE_PK")
    private BigInteger idAtiendePk;
    @Id
    @Column(name = "ID_MEDICO_FK")
    @JoinTable(
        name = "rel_books_auths",
        joinColumns = @JoinColumn(name = "FK_BOOK", nullable = false),
        inverseJoinColumns = @JoinColumn(name="FK_AUTHOR", nullable = false)
    )
    @ManyToMany(cascade = CascadeType.ALL)
    private BigInteger idMedicoFk;
      
    @Id
    @Column(name = "ID_PACIENTE_FK")
    private BigInteger idPacienteFk;

    public Atiende() {
    }
    
    
    

    public BigInteger getIdAtiendePk() {
        return idAtiendePk;
    }

    public void setIdAtiendePk(BigInteger idAtiendePk) {
        this.idAtiendePk = idAtiendePk;
    }

}
