/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.pf.jsf.beans;

import com.cognos.javap15.pf.bl.UsuarioBL;
import com.cognos.javap15.pf.model.Medico;
import com.cognos.javap15.pf.model.Paciente;
import com.cognos.javap15.pf.model.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author JAVA
 */
@Named
@RequestScoped
public class UsuarioBean implements Serializable{

    @Inject
    private UsuarioBL usuarioBL;
    
    private Paciente newPaciente;
    
    private Medico medico;
    
    private Paciente paciente;

    private List<Usuario> usuarios;
    private Usuario usuarioNuevo;

    @PostConstruct
    public void init() {
        medico = new Medico();
        medico.setNombre("Andrea");
        medico.setApellidos("Costas");
        medico.setCodigo("00-1");
        medico.setTelefono("73598371");
        usuarioBL.registrarMedico(medico);
        
        paciente = new Paciente();
        paciente.setNombre("Henry");
        paciente.setApellidos("Coarite");
        paciente.setCodigo("21653");
        usuarioBL.registrarPaciente(paciente);
        
        
        actualizarUsuarios();
        usuarioNuevo = new Usuario();
    }

    public void registrarPaciente(){
        usuarioBL.registrarPaciente(newPaciente);
    }
    
    
    public void modificarPaciente(){
    usuarioBL.modificarPaciente(newPaciente);}
    public void actualizarUsuarios() {
        usuarios = usuarioBL.listarUsuarios();
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public Usuario getUsuarioNuevo() {
        return usuarioNuevo;
    }

    public void setUsuarioNuevo(Usuario usuarioNuevo) {
        this.usuarioNuevo = usuarioNuevo;
    }

    public void registrarUsuario() {
        System.out.println("nuevo");
        usuarioBL.registrarUsuario(usuarioNuevo);
        usuarioNuevo = new Usuario();
        actualizarUsuarios();
    }

    public void modificarUsuario() {
        System.out.println("editar" + usuarioNuevo.getId());
        usuarioBL.modificarUsuario(usuarioNuevo);
        usuarioNuevo = new Usuario();
        actualizarUsuarios();
    }

    public void seleccionarUsuario(Usuario usuario) {
        System.out.println("---" + usuario.getId());
        this.usuarioNuevo = usuario;

    }

    public UsuarioBL getUsuarioBL() {
        return usuarioBL;
    }

    public void setUsuarioBL(UsuarioBL usuarioBL) {
        this.usuarioBL = usuarioBL;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Paciente getNewPaciente() {
        return newPaciente;
    }

    public void setNewPaciente(Paciente newPaciente) {
        this.newPaciente = newPaciente;
    }

}
